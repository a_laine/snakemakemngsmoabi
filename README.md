**Auteur : Antoine LAINE - Master 2 Bioinformatique (Nantes)**

**Projet : Intégration d'un pipeline d'analyse de données de métagénomique globale pour le diagnostic clinique**

**Derniere MaJ : 30/08/19**


Ce pipeline travail à partir de données fastq appariées

En l'état, le pipeline génère une archive pour chaque échantillon, contenant : 


    -fichier "XXX.info.txt"
    -répertoire "Bacteria" 
        -fichier "XXX.count.txt"
        -fichier "XXX.fastarecupDone"
        -répertoire "XXXfasta"
            -potentiels fichiers fasta
    -répertoire "Viruses"
        -fichier "XXX.count.txt"
        -fichier "XXX.fastarecupDone"
        -répertoire "XXXfasta"
            -potentiels fichiers fasta
        -répertoire "Depth_GraphXXX"
            -potentiels fichiers png
            
            
Exemple : 

En lançant le pipeline sur l'échantillon présent à l'adresse : /scratch/recherche/alaine/tmp/input

Les sorties générées devraient être celles présentes dans /scratch/recherche/alaine/tmp/output



**Docker**

Images Docker disponibles avant intégration (et version utilisée) :

    -BBmap (clumpify) : 38.23-1
    -Trimmomatic : 0.36-1
    -Kraken2 : 2.0.7-1
    -Seqkit (alternative à seqkt utilisé à l’origine) : 0.7.2-1
    -Blast (alternative à Blast+) : 2.6.0-1

Images Docker mises en place pour l’intégration :

    -Metagenomic-utilis-sls : 0.0.2-1
    · Comprend l’outil Python avec les packages ete3 et MatplotLib
    · Comprend également les 4 scripts :
        o GetIntFasta3.py : Python
        o getNames.py : Python / ete3
        o CreateDepthPlot.py : Python / matplotlib & ete3
        o SortBlastedSeq.py : Python / ete3
    Ces scripts ont été adaptés, et nécessitent des paramètres spécifiques :
        o Chaque script possède un paramètre input (-i) pour le/les fichiers d’entrée
        o Chaque script possède un paramètre output (-o) pour le/les fichiers de sortie
        o GetIntFasta3.py possède un paramètre -k obligatoire correspondant au règne taxonomique
        o SortBlastedSeq.py possède un paramètre -m facultatif correspondant au nombre de reads minimum requis pour dessiner le graph de couverture (5 par défaut) 



**Futurs additions**

Une règle Snakemake supplémentaire, utilisant le paquet Jinja2 pour créer un rapport HTML pour chaque échantillon

Ce fichier HTML devra être ajouté dans l'archive, à sa racine, pour être mis à disposition de l'utilisateur

La règle est déjà écrite dans le Snakefile (html_report), mais n'est pour l'instant pas prise en compte. Pour la lancer avec le reste :

    -modifier l'image Docker associée dans le fichier cluster.json
    -ajouter l'input **   count = "{prefix}/{sample}/{sample}.html"   ** dans la règle archive