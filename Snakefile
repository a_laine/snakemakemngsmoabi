configfile: "config.json"
workdir: config["general_path"]["OUTPUT_PATH"]

output_path = config["general_path"]["OUTPUT_PATH"]
mate_ids = ["R1","R2"]

sample_ids = []
for sample_id in config["samples"].keys():
        sample_ids.append(sample_id)
kingdom_ids = ["Viruses","Bacteria"]


#========================= Output files =============================#
import_fastq = expand((
	output_path + "/{sample_id}/{sample_id}R1.fastq.gz",
	output_path + "/{sample_id}/{sample_id}R2.fastq.gz"),
	sample_id = sample_ids
)

dedupe = expand((
	output_path + "/{sample_id}/{sample_id}R1_dedupe.fastq.gz",
	output_path + "/{sample_id}/{sample_id}R2_dedupe.fastq.gz"),
	sample_id = sample_ids
)

trimmomatic = expand((
	output_path + "/{sample_id}/{sample_id}R1_paired.fq.gz",
	output_path + "/{sample_id}/{sample_id}R2_paired.fq.gz",
    output_path + "/{sample_id}/{sample_id}R1_unpaired.fq.gz",
    output_path + "/{sample_id}/{sample_id}R2_unpaired.fq.gz"),
	sample_id = sample_ids
)

kraken2 = expand((
    output_path + "/{sample_id}/{sample_id}.report.txt",
    output_path + "/{sample_id}/{sample_id}.output.txt",
    output_path + "/{sample_id}/{sample_id}.clseqs_1.fastq",
    output_path + "/{sample_id}/{sample_id}.clseqs_2.fastq"),
    sample_id = sample_ids
)

infofile = expand((
    output_path + "/{sample_id}/{sample_id}.info.txt"),
    sample_id = sample_ids
)

get_readnames = expand((
    output_path + "/{sample_id}/{kingdom_id}/{sample_id}.report.txtReadsList.txt"),
    sample_id = sample_ids,
    kingdom_id = kingdom_ids
)

get_int_fasta = expand((
    output_path + "/{sample_id}/{kingdom_id}/{sample_id}.interesting.fasta"),
    sample_id = sample_ids,
    kingdom_id = kingdom_ids
)

blast = expand((
    output_path + "/{sample_id}/{kingdom_id}/{sample_id}.blast.txt"),
    sample_id = sample_ids,
    kingdom_id = kingdom_ids
)

sort_blast = expand((
    output_path + "/{sample_id}/{kingdom_id}/{sample_id}.conserved.txt",
    output_path + "/{sample_id}/{kingdom_id}/{sample_id}.notconserved.txt"),
    sample_id = sample_ids,
    kingdom_id = kingdom_ids
)

counting = expand((
    output_path + "/{sample_id}/{kingdom_id}/{sample_id}.counting.txt"),
    sample_id = sample_ids,
    kingdom_id = kingdom_ids
)

vir_analysis = expand((
    output_path + "/{sample_id}/Viruses/{sample_id}.count.txt"),
    sample_id = sample_ids
)

bact_analysis = expand((
    output_path + "/{sample_id}/Bacteria/{sample_id}.count.txt"),
    sample_id = sample_ids
)

get_fasta_final = expand((
    output_path + "/{sample_id}/{kingdom_id}/{sample_id}.fastarecupDone"),
    sample_id = sample_ids,
    kingdom_id = kingdom_ids
)

html_report= expand((
    output_path + "/{sample_id}/{sample_id}.html"),
    sample_id = sample_ids
)

archive = expand((
    output_path + "/{sample_id}.tar.gz"),
    sample_id = sample_ids
)
#========================= RULES ======================#

rule all:
    input:
        archive
    shell:
        "touch "+output_path+"/done"

rule copytoTMP:
    input:
        R1 = lambda wildcards: config["samples"][wildcards.sample]["R1_path"],
        R2 = lambda wildcards: config["samples"][wildcards.sample]["R2_path"]
    output:
        R1 = temp("{prefix}/{sample}/{sample}R1.fastq.gz"),
        R2 = temp("{prefix}/{sample}/{sample}R2.fastq.gz")
    log:
        "{prefix}/logs/{sample}.copytoTMP.log"
    shell:
        'cp {input.R1} {output.R1} 2>&1 | tee -a {log};\
        cp {input.R2} {output.R2} 2>&1 | tee -a {log}'

rule dedupe:
    input:
        R1 = "{prefix}/{sample}/{sample}R1.fastq.gz",
        R2 = "{prefix}/{sample}/{sample}R2.fastq.gz"
    output:
        R1 = temp("{prefix}/{sample}/{sample}R1_dedupe.fastq.gz"),
        R2 = temp("{prefix}/{sample}/{sample}R2_dedupe.fastq.gz")
    log:
        "{prefix}/logs/{sample}.dedupe.log"
    params:
        dedupe_qin = config["dedupe"]["OPTIONS"]
    shell:
        'java -ea -Xmx9g -Xms9g -XX:ParallelGCThreads=10 -cp /usr/share/bbmap/java clump.Clumpify {params.dedupe_qin} \
            in1={input.R1} in2={input.R2} \
            out1={output.R1} out2={output.R2} \
            dedupe | tee -a {log}'

rule trimmomatic:
    input:
        R1 = "{prefix}/{sample}/{sample}R1_dedupe.fastq.gz",
        R2 = "{prefix}/{sample}/{sample}R2_dedupe.fastq.gz"
    output:
        R1 = temp("{prefix}/{sample}/{sample}R1_paired.fq.gz"),
        R2 = temp("{prefix}/{sample}/{sample}R2_paired.fq.gz"),
        R1u = temp("{prefix}/{sample}/{sample}R1_unpaired.fq.gz"),
        R2u = temp("{prefix}/{sample}/{sample}R2_unpaired.fq.gz")
    log:
        "{prefix}/logs/{sample}.trimmomatic.log"
    params:
        trimmo_options = config["trimmomatic"]["OPTIONS"],
        trimmo_params = config["trimmomatic"]["PARAMS"]
    shell:
        'java -Xmx2800m -Xms2800m -jar /usr/share/java/trimmomatic-0.36.jar \
        {params.trimmo_options} {input.R1} {input.R2} \
        {output.R1} {output.R1u} {output.R2} {output.R2u} \
        {params.trimmo_params} | tee -a {log}'

rule kraken2:
    input:
        R1 = "{prefix}/{sample}/{sample}R1_paired.fq.gz",
        R2 = "{prefix}/{sample}/{sample}R2_paired.fq.gz"
    output:
        outputK = temp("{prefix}/{sample}/{sample}.output.txt"),
        reportK = temp("{prefix}/{sample}/{sample}.report.txt"),
        clseqs1= temp("{prefix}/{sample}/{sample}.clseqs_1.fastq"),
        clseqs2 = temp("{prefix}/{sample}/{sample}.clseqs_2.fastq"),
        unclseqs1= temp("{prefix}/{sample}/{sample}.unclseqs_1.fq"),
        unclseqs2 = temp("{prefix}/{sample}/{sample}.unclseqs_2.fq")
    log:
        "{prefix}/logs/{sample}.kraken2.log"
    params:
        kraken2_options = config["kraken2"]["OPTIONS"],
        kraken2_db = config["kraken2"]["DATABASE"]
    shell:
        'kraken2 {params.kraken2_db} {params.kraken2_options} \
        --report {output.reportK} --output {output.outputK} \
        --classified-out {wildcards.prefix}/{wildcards.sample}/{wildcards.sample}.clseqs#.fastq \
        --unclassified-out {wildcards.prefix}/{wildcards.sample}/{wildcards.sample}.unclseqs#.fq \
        {input.R1} {input.R2} | tee -a {log}'

rule infofile:
    input:
        R1="{prefix}/{sample}/{sample}R1.fastq.gz",
        reportL="{prefix}/{sample}/{sample}.report.txt"
    output:
        "{prefix}/{sample}/{sample}.info.txt"
    log:
        "{prefix}/logs/{sample}.infofile.log"
    shell:
        """
        countReads=$(zcat {input.R1} | grep '^+$' | wc -l )
        unclassifiedReads=$(head -2 {input.reportL} | cut -f2 | head -1)
        classifiedReads=$(head -2 {input.reportL} | cut -f2 | tail -1)
        humanReads=$(grep 'Homo sapiens' {input.reportL} | cut -f2 | head -1)
        bacterialReads=$(grep 'Bacteria' {input.reportL} | cut -f2 | head -1)
        viralReads=$(grep 'Viruses' {input.reportL} | cut -f2 | head -1)
        echo $(($countReads * 2)) > {output}
        echo $(($unclassifiedReads * 2 + $classifiedReads * 2)) >> {output}
        echo $(($classifiedReads * 2)) >> {output}
        if [ -z "$humanReads" ];
        then
          echo "0" >> {output}
        else
          echo $(($humanReads * 2)) >> {output}
        fi
        if [ -z "$bacterialReads" ];
        then
          echo "0" >> {output}
        else
          echo $(($bacterialReads * 2)) >> {output}
        fi
        if [ -z "$viralReads" ];
        then
          echo "0" >> {output}
        else
          echo $(($viralReads * 2)) >> {output}
        fi
        """

rule get_readnames:
    input:
        outputK = "{prefix}/{sample}/{sample}.output.txt",
        reportK = "{prefix}/{sample}/{sample}.report.txt"
    output:
        readnames = temp("{prefix}/{sample}/{taxkingdom}/{sample}.report.txtReadsList.txt")
    log:
        "{prefix}/logs/{sample}_get_readnames.log"
    shell:
        'GetIntFasta3.py -i {input.reportK} -o {output.readnames} -k {wildcards.taxkingdom} | tee -a {log}'

rule get_int_fasta:
    input:
        readnames = "{prefix}/{sample}/{taxkingdom}/{sample}.report.txtReadsList.txt",
        clseqs1 = "{prefix}/{sample}/{sample}.clseqs_1.fastq",
        clseqs2 = "{prefix}/{sample}/{sample}.clseqs_2.fastq"
    output:
        temp("{prefix}/{sample}/{taxkingdom}/{sample}.interesting.fasta")
    log:
        "{prefix}/logs/{sample}_get_int_fasta.log"
    shell:
        #'RecoverReads.sh {input.readnames} {input.clseqs1} {input.clseqs2} {output} | tee -a {log}'
        """
        temp_file=$(mktemp)
        temp_fasta=$(mktemp)
        cat {input.clseqs1} | seqkit grep -f {input.readnames} >  $temp_file || true
        seqkit fq2fa $temp_file -o $temp_fasta

        if [ $? -eq 0 ]; then
            rm $temp_file
            if [[ -s $temp_fasta ]]
            then
                echo "Reads recovered for {input.clseqs1}"
            else
            echo "Reads not recovered for {input.clseqs1}"
            fi
        else
            echo "FAIL for {input.clseqs1}"
        fi

        temp_file1=$(mktemp)
        temp_fasta1=$(mktemp)
        cat {input.clseqs2}  | seqkit grep -f {input.readnames} >  $temp_file1 || true
        seqkit fq2fa $temp_file1 -o $temp_fasta1

        if [ $? -eq 0 ]; then
            rm $temp_file1
            if [[ -s $temp_fasta1 ]]
            then
            echo "Reads recovered for {input.clseqs2}"
            else
            echo "Reads not recovered for {input.clseqs2}"
            fi
        else
            echo "FAIL for {input.clseqs2}"
        fi

        if [[ -s $temp_fasta && -s $temp_fasta1 ]]
        then
            cat $temp_fasta $temp_fasta1  >  {output}
            rm $temp_fasta
            rm $temp_fasta1
        elif [[ ! -s $temp_fasta && -s $temp_fasta1 ]]
        then
           mv $temp_fasta1 {output}
        elif [[ -s $temp_fasta && ! -s $temp_fasta1 ]]
        then
           mv $temp_fasta {output}
        elif [[ ! -s $temp_fasta && ! -s $temp_fasta1 ]]
        then
           echo "Non reads recovered at all"
        fi
        """

rule blast:
    input:
        "{prefix}/{sample}/{taxkingdom}/{sample}.interesting.fasta"
    output:
        temp("{prefix}/{sample}/{taxkingdom}/{sample}.blast.txt")
    log:
        "{prefix}/logs/{sample}_blast.log"
    params:
        blast_options = config["blast"]["OPTIONS"],
        blast_db = lambda w: config["blast"]["DATABASE-{}".format(w.taxkingdom)]
    shell:
        'blastn {params.blast_db} {params.blast_options} -query {input} > {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}.blasttemp.txt | tee -a {log};\
        tac {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}.blasttemp.txt | sed \'/0 hits/I,+3 d\' |tac > {output} | tee -a {log};\
        rm {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}.blasttemp.txt'

rule sort_blast:
    input:
        "{prefix}/{sample}/{taxkingdom}/{sample}.blast.txt"
    output:
        conserved = temp("{prefix}/{sample}/{taxkingdom}/{sample}.conserved.txt"),
        notconserved = temp("{prefix}/{sample}/{taxkingdom}/{sample}.notconserved.txt")
    log:
        "{prefix}/logs/{sample}_sort_blast.log"
    shell:
        'SortBlastedSeq.py -i {input} -o {output.conserved} -n /data/annotations/Blast/12_08_2019_custom/NCBITaxa/.etetoolkit/taxa.sqlite | tee -a {log}'

rule vir_counting:
    input:
        conserved="{prefix}/{sample}/Viruses/{sample}.conserved.txt",
	    blast="{prefix}/{sample}/Viruses/{sample}.blast.txt"
    output:
        temp("{prefix}/{sample}/Viruses/{sample}.counting.txt")
    log:
        "{prefix}/logs/{sample}_vircounting.log"
    shell:
        'sort -n {input.conserved} -k8 > {wildcards.prefix}/{wildcards.sample}/Viruses/{wildcards.sample}.sorted.txt;\
	rm {input.conserved};\
	mv {wildcards.prefix}/{wildcards.sample}/Viruses/{wildcards.sample}.sorted.txt {input.conserved};\
	cut -f8 {input.conserved} |uniq -c > {wildcards.prefix}/{wildcards.sample}/Viruses/{wildcards.sample}.temp1.txt;\
	cut -f8 {input.conserved} |uniq > {wildcards.prefix}/{wildcards.sample}/Viruses/{wildcards.sample}.temp2.txt;\
	while read p; do echo -n $p" " >> {wildcards.prefix}/{wildcards.sample}/Viruses/{wildcards.sample}.temp3.txt;grep "taxid|"$p {input.blast} |wc -l >> {wildcards.prefix}/{wildcards.sample}/Viruses/{wildcards.sample}.temp3.txt || true;done<{wildcards.prefix}/{wildcards.sample}/Viruses/{wildcards.sample}.temp2.txt;\
	join -1 2 -2 1 {wildcards.prefix}/{wildcards.sample}/Viruses/{wildcards.sample}.temp1.txt {wildcards.prefix}/{wildcards.sample}/Viruses/{wildcards.sample}.temp3.txt |sort -k2,2 -gr > {output};\
    rm {wildcards.prefix}/{wildcards.sample}/Viruses/{wildcards.sample}.temp1.txt {wildcards.prefix}/{wildcards.sample}/Viruses/{wildcards.sample}.temp2.txt {wildcards.prefix}/{wildcards.sample}/Viruses/{wildcards.sample}.temp3.txt'

rule bact_counting:
    input:
        conserved="{prefix}/{sample}/Bacteria/{sample}.conserved.txt",
        blast="{prefix}/{sample}/Bacteria/{sample}.blast.txt"
    output:
        temp("{prefix}/{sample}/Bacteria/{sample}.counting.txt")
    log:
        "{prefix}/logs/{sample}_bactcounting.log"
    shell:
        'sort -n {input.conserved} -k8 > {wildcards.prefix}/{wildcards.sample}/Bacteria/{wildcards.sample}.sorted.txt;\
        rm {input.conserved};\
        mv {wildcards.prefix}/{wildcards.sample}/Bacteria/{wildcards.sample}.sorted.txt {input.conserved};\
        cut -f8 {input.conserved} | uniq -c |sort -k2,2 -g > {wildcards.prefix}/{wildcards.sample}/Bacteria/{wildcards.sample}.temp1.txt;\
        cut -f2,8 {input.conserved} | sort -k1,1 | uniq | cut -f2 | sort -g | uniq -c > {wildcards.prefix}/{wildcards.sample}/Bacteria/{wildcards.sample}.temp2.txt;\
        join -1 2 -2 2 {wildcards.prefix}/{wildcards.sample}/Bacteria/{wildcards.sample}.temp1.txt {wildcards.prefix}/{wildcards.sample}/Bacteria/{wildcards.sample}.temp2.txt |sort -k1,1b > {wildcards.prefix}/{wildcards.sample}/Bacteria/{wildcards.sample}.temp3.txt;\
        join -1 1 -2 1 {wildcards.prefix}/{wildcards.sample}/Bacteria/{wildcards.sample}.temp3.txt /data/annotations/Blast/12_08_2019_custom/MetaPhlAn_DB/totalCountofGenes.txt | sort -k2,2 -gr > {output};\
        rm {wildcards.prefix}/{wildcards.sample}/Bacteria/{wildcards.sample}.temp1.txt {wildcards.prefix}/{wildcards.sample}/Bacteria/{wildcards.sample}.temp2.txt {wildcards.prefix}/{wildcards.sample}/Bacteria/{wildcards.sample}.temp3.txt'

rule vir_analysis:
    input:
        conserved = "{prefix}/{sample}/Viruses/{sample}.conserved.txt",
        counting = "{prefix}/{sample}/Viruses/{sample}.counting.txt"
    output:
        count = "{prefix}/{sample}/Viruses/{sample}.count.txt"
    log:
        "{prefix}/{sample}/logs/{sample}_vir_analysis.log"
    params:
        vir_options = config["vir_analysis"]["OPTIONS"]
    shell:
        'CreateDepthPlot.py -i {input.conserved} {input.counting} -o {output.count} -n /data/annotations/Blast/12_08_2019_custom/NCBITaxa/.etetoolkit/taxa.sqlite {params.vir_options} '

rule bact_analysis:
    input:
        counting = "{prefix}/{sample}/Bacteria/{sample}.counting.txt"
    output:
        count = "{prefix}/{sample}/Bacteria/{sample}.count.txt"
    log:
        "{prefix}/{sample}/logs/{sample}_bact_analysis.log"
    shell:
        'getNames.py -i {input.counting} -o {output.count} -n /data/annotations/Blast/12_08_2019_custom/NCBITaxa/.etetoolkit/taxa.sqlite'

rule get_fasta_final:
    input:
        conserved = "{prefix}/{sample}/{taxkingdom}/{sample}.conserved.txt",
        clseqs1 = "{prefix}/{sample}/{sample}.clseqs_1.fastq",
        clseqs2 = "{prefix}/{sample}/{sample}.clseqs_2.fastq"
    output:
        temp("{prefix}/{sample}/{taxkingdom}/{sample}.fastarecupDone")
    log:
        "{prefix}/{sample}/logs/{sample}_get_fasta_final.log"
    shell:
        """
	    mkdir -p {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta
        cat {input.conserved} |awk -v pathF="{wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta" -F"[\t]" '$10~/^1/ {{print $1" "$8 > pathF"/map1.fa" ; print $1 > pathF"/1.fa" }}'
        cat {input.conserved} |awk -v pathF="{wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta" -F"[\t]" '$10~/^2/ {{print $1" "$8 > pathF"/map2.fa" ; print $1 > pathF"/2.fa" }}'
        temp_file=$(mktemp)
        temp_fasta=$(mktemp)
        cat {input.clseqs1}  | seqkit grep -f {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/1.fa > $temp_file || true
        seqkit fq2fa $temp_file -o $temp_fasta
        if [[ -s $temp_fasta ]];
        then
            mv $temp_fasta {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/1.fasta
        else
            touch {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/1.fasta
        fi
        temp_file1=$(mktemp)
        temp_fasta1=$(mktemp)
        cat {input.clseqs2}  | seqkit grep -f {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/2.fa > $temp_file1 || true
        seqkit fq2fa $temp_file1 -o $temp_fasta1
        if [[ -s $temp_fasta1 ]];
        then
            mv $temp_fasta1 {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/2.fasta
        else
            touch {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/2.fasta
        fi
        cat {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/1.fasta | paste - - | cut -c2- |sort > {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/sorted1.fasta
        cat {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/2.fasta | paste - - | cut -c2- |sort > {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/sorted2.fasta
        sort {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/map1.fa > {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/sorted1.fa
        sort {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/map2.fa > {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/sorted2.fa
        join -11 -21 {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/sorted1.fasta {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/sorted1.fa > {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/1.fasta
        join -11 -21 {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/sorted2.fasta {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/sorted2.fa > {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/2.fasta
        cat {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/1.fasta |awk -v pathF="{wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/" '{{print ">"$1" "$2" "$3"\\n"$4 > pathF"/"$5".fasta"}}'
        cat {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/2.fasta |awk -v pathF="{wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/" '{{print ">"$1" "$2" "$3"\\n"$4 >> pathF"/"$5".fasta"}}'
        rm {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/1.fasta {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/2.fasta {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/1.fa {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/2.fa
        rm {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/map1.fa {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/map2.fa {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/sorted1.fa {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/sorted2.fa
        rm {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/sorted1.fasta {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}fasta/sorted2.fasta
    	touch {wildcards.prefix}/{wildcards.sample}/{wildcards.taxkingdom}/{wildcards.sample}.fastarecupDone
        """

rule html_report:
    input:
        info = "{prefix}/{sample}/{sample}.info.txt"
    output:
        count = "{prefix}/{sample}/{sample}.html"
    log:
        "{prefix}/{sample}/logs/{sample}_html_report.log"
    shell:
        'reportSingleSample.py -i {input} -o {output} -t templates'
        
rule archive:
    input:
        fastaDoneB = "{prefix}/{sample}/Bacteria/{sample}.fastarecupDone",
        fastaDoneV = "{prefix}/{sample}/Viruses/{sample}.fastarecupDone",
        countFileB = "{prefix}/{sample}/Bacteria/{sample}.count.txt",
        countFileV = "{prefix}/{sample}/Viruses/{sample}.count.txt",
        infoFile = "{prefix}/{sample}/{sample}.info.txt"

    output:
        zip = "{prefix}/{sample}.tar.gz"
    shell:
        'cd {wildcards.prefix};\
        tar -zcvf {wildcards.sample}.tar.gz {wildcards.sample}/'



